<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */
class Poebel_MagentoLiteBootstrap_Block_Rewrite_Page_Html_Topmenu extends Mage_Page_Block_Html_Topmenu
{
    const IS_IN_DROPDOWN = 'isInDropdown';

    /**
     * Recursively generates top menu html from data that is specified in $menuTree
     *
     * @param Varien_Data_Tree_Node $menuTree
     * @param string                $childrenWrapClass
     *
     * @return string
     */
    protected function _getHtml(Varien_Data_Tree_Node $menuTree, $childrenWrapClass)
    {
        $html = '';

        $children = $menuTree->getChildren();

        foreach ($children as $child) {
            if ($child->getUrl() == '#divider') {
                $html .= '<li class="divider"></li>';
            } elseif ($child->getIsDropdownHeader()) {
                $html .= '<li class="dropdown-header">' . $this->escapeHtml($child->getName()) . '</li>';
            } else {
                if ($child->hasChildren()) {
                    if ($childrenWrapClass != self::IS_IN_DROPDOWN) {
                        $html .= '<li class="dropdown' . ($child->getIsActive() ? ' active' : '') . '">';
                        $html .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' . $this->escapeHtml($child->getName()) . ' <c class="caret"></b></a>';
                        $html .= '<ul class="dropdown-menu">';

                        if ($child->getUrl() != '#') {
                            $html .= '<li' . ($child->getIsActive() ? ' class="active"' : '') . '>';
                            $html .= '<a href="' . $child->getUrl() . '">' . $this->escapeHtml($child->getName()) . '</a>';
                            $html .= '</li>';
                            $html .= '<li class="divider"></li>';
                        }

                        $html .= $this->_getHtml($child, self::IS_IN_DROPDOWN);

                        $html .= '</ul>';
                        $html .= '</li>';
                    } else {
                        $html .= '<li' . ($child->getIsActive() ? ' class="active"' : '') . '>';
                        $html .= '<a href="' . $child->getUrl() . '">' . $this->escapeHtml($child->getName()) . '</a>';
                        $html .= '</li>';
                        $html .= $this->_getHtml($child, $childrenWrapClass);
                    }
                } else {
                    $html .= '<li' . ($child->getIsActive() ? ' class="active"' : '') . '>';
                    $html .= '<a href="' . $child->getUrl() . '">' . $this->escapeHtml($child->getName()) . '</a>';
                    $html .= '</li>';
                }
            }
        }

        return $html;
    }
}