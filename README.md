# Bootstrap 3 Theme for Magento Lite  
  
## Installation:  
  
1. Install Magento Lite: https://github.com/colinmollenhour/magento-lite  
2. Install this module  
3. Go to "System -> Configuration -> General -> Design" and  
   change "Package -> Current Package Name" to "magentolitebootstrap"  
4. Refresh your cache
